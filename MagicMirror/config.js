/* MagicMirror² Config Sample
 *
 * By Michael Teeuw https://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information on how you can configure this file
 * see https://docs.magicmirror.builders/configuration/introduction.html
 * and https://docs.magicmirror.builders/modules/configuration.html
 *
 * You can use environment variables using a `config.js.template` file instead of `config.js`
 * which will be converted to `config.js` while starting. For more information
 * see https://docs.magicmirror.builders/configuration/introduction.html#enviromnent-variables
 */
let config = {
	address: "localhost",	// Address to listen on, can be:
							// - "localhost", "127.0.0.1", "::1" to listen on loopback interface
							// - another specific IPv4/6 to listen on a specific interface
							// - "0.0.0.0", "::" to listen on any interface
							// Default, when address config is left out or empty, is "localhost"
	port: 8080,
	basePath: "/",			// The URL path where MagicMirror² is hosted. If you are using a Reverse proxy
					  		// you must set the sub path here. basePath must end with a /
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"],	// Set [] to allow all IP addresses
															// or add a specific IPv4 of 192.168.1.5 :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
															// or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	useHttps: false, 		// Support HTTPS or not, default "false" will use HTTP
	httpsPrivateKey: "", 	// HTTPS private key path, only require when useHttps is true
	httpsCertificate: "", 	// HTTPS Certificate path, only require when useHttps is true

	language: "en",
	locale: "en-US",
	logLevel: ["INFO", "LOG", "WARN", "ERROR"], // Add "DEBUG" for even more logging
	timeFormat: 12,
	units: "imperial",

	modules: [
		{
			module: "alert",
		},
		{
			module: "updatenotification",
			position: "top_bar"
		},
		{
			module: "clock",
			position: "top_left",
			config: {
				timezone: "America/Chicago",
				showPeriodUpper: true
			}
		},
		//{
		//	module: "calendar",
		//	header: "US Holidays",
		//	position: "top_left",
		//	config: {
		//		calendars: [
		//			{
		//				symbol: "calendar-check",
		//				url: "webcal://www.calendarlabs.com/ical-calendar/ics/76/US_Holidays.ics"
		//			}
		//		]
		//	}
		//},
		{
			module: "weather",
			position: "top_right",
			config: {
				weatherProvider: "openweathermap",
				type: "current",
				onlyTemp: true,
				roundTemp: true,
				degreeLabel: true,
				showPrecipitationAmount: true,
				showPrecipitationProbability: true,
				location: "Champaign",
				locationID: "4887158", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				apiKey: "254f8ef2ac370535f9656b6ca5dcd6cb"
			}
		},
		{
			module: "weather",
			position: "top_right",
			header: "Weather Forecast",
			config: {
				weatherProvider: "openweathermap",
				type: "forecast",
				location: "Champaign",
				locationID: "4887158", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				apiKey: "254f8ef2ac370535f9656b6ca5dcd6cb",
				initialLoadDelay: 5000
			}
		},
		//{
		//	module: "newsfeed",
		//	position: "bottom_bar",
		//	config: {
		//		feeds: [
		//			{
		//				title: "New York Times",
		//				url: "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml"
		//			}
		//		],
		//		showSourceTitle: true,
		//		showPublishDate: true,
		//		broadcastNewsFeeds: true,
		//		broadcastNewsUpdates: true
		//	}
		//},
		{
			module: "MMM-SleepWake",
			config: {
				delay: 60,
			},
		},
		{
			module: "MMM-LOLESPORTS-SCHEDULES",
			position: "bottom_left",
			config: {
				leagueId: ["98767991325878492"],
				showTeamLabel: false,
			},
		},
		{
			module: "MMM-OnSpotify",
			position: "bottom_right",
			config: {
				clientID: "f16c28ef244546f896cfd97bb598fab0",
				clientSecret: "73df633c9cc5465c8e9212525ffa20f5",
				accessToken: "BQCOa5Aarbu-3UusV1AFheuYO1dv3CGClTo-YiUdRM5cea_3MJTHPnXI_MRU-O8j09QtTmOHwCYH904kraJ99rWx96DSVt5aSh3AwLJ007S0t1_LrEaCNDBcyIjJeSSwlsaiWEZopy1paNzwuYI6HSB-oEN3K5xRvlYaPMUB90ySQuf9cv6splWFIKE7NggHZ-oJZkPKQZxA",
				refreshToken: "AQD-UnWUb9KbDbtp8WD87PyCL3XlH7Gb1xZfaxpDYBW9iTfaJsUQSutcyLBBhtlyZUvc7N4GaD2lDNkHvPSkZvbXBAPc7YCJIGKh065vzAClfJiMfIrOaUY4PZx1FKg9WLk",
				displayWhenEmpty: "both",
				userAffinityUseTracks: true,
				spotifyCodeExperimentalShow: false,
				showBlurBackground: false,
			},
		},
		{
			module: "MMM-EyeCandy",
			position: "middle_center",
			config: {
				ownImagePath: 'modules/MMM-EyeCandy/pix/pepe_dance.gif',
			}
		},
	]
};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
